extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	for buttons in $Menu/Center/Row/Buttons.get_children():
		buttons.connect("pressed", self, "_on_Buttons_pressed", [button.scene_to_load])
		
func _on_Buttons_pressed(scene_to_load):
	get_tree().change_scene(scene_to_load)
